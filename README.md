# dgb - Dash Git Build

Package manager that runs dash-scripts to compile/install/update/remove programs. 

The idee of this that you add any program of your wish and install/update/remove it with this tool.

The Frontend Program should check dependencies and run the scripts needed to install a program on any unix-like system.

## Design
```
~/dgb/bin/                            | Binary Installation Folder
~/dgb/dgb-scripts/programname/action  | dash-scripts for installing/building, updating and removing programs
~/dgb/env                             | adds ~/dgb/bin/ to PATH
~/dgb/setup                           | copies dgb-script to ~/dgb/bin/
~/.repos/                             | Git-Repos of installed programs
```

TODO: Change to env variables
```
DGBROOT=$HOME/dgb
DGBREPOS=$HOME/.repos
DGBSCRIPTS=$HOME/dgb/dgb-scripts
DGBBIN=$HOME/dgb/bin
```

## Installation 

* `git clone https://gitlab.com/HamsterLovesCompiling/dgb ~/dgb` 
* add `source $HOME/dgb/env` to your .profile or add `~/dgb/bin/` to `PATH`
* run `~/dgb/setup`

## Usage

```sh
dgb install programname # installs program
dgb remove programname  # removes program
dgb update programname  # updates program
dgb update              # updates all programs
```

